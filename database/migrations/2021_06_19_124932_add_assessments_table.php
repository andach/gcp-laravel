<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->boolean('is_complete')->nullable();
            $table->timestamps();
        });
        
        Schema::create('assessments_responses', function (Blueprint $table) {
            $table->id();
            $table->integer('assessment_id');
            $table->integer('question_id');
            $table->timestamps();
        });

        Schema::create('assessments_questions', function (Blueprint $table) {
            $table->id();
            $table->nestedSet();
            $table->integer('next_question_id')->nullable();
            $table->integer('order');
            $table->string('enum_assessment_category');
            $table->string('name');
            $table->string('answer_type');
            $table->timestamps();
        });

        Schema::create('assessments_questions_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('question_id');
            $table->integer('next_question_id')->nullable();
            $table->string('name');
            $table->double('score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
        Schema::dropIfExists('assessments_responses');
        Schema::dropIfExists('assessments_questions');
        Schema::dropIfExists('assessments_questions_answers');
    }
}
