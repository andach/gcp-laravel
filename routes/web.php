<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout-get');

Route::get('/auth/{driver}/redirect', function ($driver) {
    return Socialite::driver($driver)->redirect();
});
Route::get('auth/facebook/callback', ['App\Http\Controllers\SocialController', 'loginWithFacebook']);

Route::group(['middleware' => ['auth']], function ()
{
    Route::get('/admin/company', 'App\Http\Controllers\AdminController@company')->name('admin.company');
    Route::post('/admin/company', 'App\Http\Controllers\AdminController@companyPost')->name('admin.company-post');
    
    Route::get('/admin/question/{id}', 'App\Http\Controllers\AdminController@question')->name('admin.question');
    Route::post('/admin/question/{id}', 'App\Http\Controllers\AdminController@questionPost')->name('admin.question-post');
    Route::get('/admin/questions', 'App\Http\Controllers\AdminController@questions')->name('admin.questions');
    Route::post('/admin/questions', 'App\Http\Controllers\AdminController@questionsPost')->name('admin.questions-post');

    Route::get('/assessment', 'App\Http\Controllers\AssessmentController@assessment')->name('assessment');

    Route::get('/', function () {
        return view('home');
    });
});