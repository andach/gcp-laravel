<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Register the form components. The general principle behind these components is that they add a 'title' argument at the start of the argument
         * block, and keep everything the same as the underlying LaravelCollective signature. Extra items are put into the 'attributes' argument
         * at the end, with the component automatically working out if there is any error and applying the bootstrap is-invalid accordingly. The bootstrap
         * "form-control" class is also handled automatically. 
         * 
         * By default, items are created with the label being "col-12 col-md-3", and the element is "col-12 col-md-9" accordingly. To change this, add the
         * following to the attributes array. Help text is supported similarly: (Note the textarea just defaults to "col-12" for both.)
         * 
         * labelCols   = "col-12 col-md-2"
         * elementCols = "col-12 col-md-4"
         * helpBlock   = "Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji."
         * 
         * Note finally that the checkbox has a "bsCheckboxReverse" option, which just has the checkbox first, and the label second. 
         */
        
        Form::component('bsCheckbox', 'component.form.bs.checkbox', ['title', 'name', 'value' => null, 'checked' => null, 'attributes' => []]);
        Form::component('bsCheckboxReverse', 'component.form.bs.checkbox-reverse', ['title', 'name', 'value' => null, 'checked' => null, 'attributes' => []]);
        Form::component('bsDate', 'component.form.bs.date', ['title', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsEmail', 'component.form.bs.email', ['title', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsFile', 'component.form.bs.file', ['title', 'name', 'attributes' => []]);
        Form::component('bsNumber', 'component.form.bs.number', ['title', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsPassword', 'component.form.bs.password', ['title', 'name', 'attributes' => []]);
        Form::component('bsSelect', 'component.form.bs.select', ['title', 'name', 'options', 'value' => null, 'attributes' => []]);
        Form::component('bsSubmit', 'component.form.bs.submit', ['title']);
        Form::component('bsText', 'component.form.bs.text', ['title', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsTextarea', 'component.form.bs.textarea', ['title', 'name', 'value' => null, 'attributes' => []]);
    }
}
