<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Question extends Model
{
    use HasFactory, NodeTrait;

    protected $fillable = ['parent_id', 'next_question_id', 'order', 'enum_assessment_category', 'name', 'answer_type'];
    protected $table = 'assessments_questions';

    public function answers()
    {
        return $this->hasMany('App\Models\Assessment\Answer');
    }

    public function nextQuestion()
    {
        return $this->belongsTo('App\Models\Assessment\Question', 'next_question_id');
    }
}
