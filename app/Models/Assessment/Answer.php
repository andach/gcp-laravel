<?php

namespace App\Models\Assessment;

use App\Models\Assessment\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    protected $fillable = ['question_id', 'name', 'next_question_id', 'score'];
    protected $table = 'assessments_questions_answers';

    public function getSpecificNextQuestionTextAttribute()
    {
        if (!$this->next_question_id)
        {
            return '';
        }

        return $this->nextQuestion()->name;
    }

    public function nextQuestion()
    {
        return Question::find($this->nextQuestionID());
    }

    public function nextQuestionID()
    {
        return $this->next_question_id ?? $this->question->next_question_id;
    }

    public function question()
    {
        return $this->belongsTo('App\Models\Assessment\Question');
    }
}
