<?php

namespace App\Models\Assessment;

use App\Models\Assessment\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    use HasFactory;

    protected $fillable = ['user_id'];
    protected $table = 'assessments';

    public function responses()
    {
        return $this->hasMany('App\Models\Assessment\Response');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Assessment\Question');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getCurrentQuestion()
    {
        return Question::first();
    }
}
