<?php

namespace App\Enums;

use App\Enums\Enum;

class AssessmentCategory extends Enum
{
    private const LIFE = 'Lifestyle';
    private const BODY = 'Body';
    private const MIND = 'Mind & Mental Health';
}