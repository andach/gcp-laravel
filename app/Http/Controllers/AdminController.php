<?php

namespace App\Http\Controllers;

use App\Enums\AssessmentCategory;
use App\Models\Assessment\Answer;
use App\Models\Assessment\Question;
use App\Models\Company;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function company()
    {
        $args = [];
        $args['companies']     = Company::get()->toTree();
        $args['formCompanies'] = Company::whereNull('parent_id')->pluck('name', 'id');

        return view('admin.company', $args);
    }

    public function companyPost(Request $request)
    {
        Company::fixTree();

        if ($request->name)
        {
            Company::create([
                'name' => $request->name,
                'parent_id' => $request->parent_id,
            ]);

            session()->flash('success', 'The new company or department has been added');
        }

        return redirect()->route('admin.company');
    }

    public function question($id)
    {
        $args = [];
        $args['question']       = Question::find($id);
        $args['formParents']    = Question::whereNull('parent_id')->pluck('name', 'id');
        $args['formQuestions']  = Question::pluck('name', 'id');
        $args['formCategories'] = AssessmentCategory::toArray();

        return view('admin.question', $args);
    }

    public function questionPost(Request $request, $id)
    {
        Question::fixTree();

        $question = Question::find($id);
        $question->update($request->all());

        if ($request->answer_name)
        {
            $question->answers()->create(
                [
                    'name' => $request->answer_name,
                    'score' => $request->answer_score,
                    'next_question_id' => $request->next_question_id,
                ]
            );
        }

        if ($request->delete_answers)
        {
            foreach ($request->delete_answers as $answerID)
            {
                Answer::destroy($answerID);
            }
        }

        session()->flash('success', 'The question and answers have been updated');

        return redirect()->route('admin.question', $id);
    }

    public function questions()
    {
        $args = [];
        $args['questions']      = Question::all();
        $args['formParents']    = Question::whereNull('parent_id')->pluck('name', 'id');
        $args['formQuestions']  = Question::pluck('name', 'id');
        $args['formCategories'] = AssessmentCategory::toArray();

        return view('admin.questions', $args);
    }

    public function questionsPost(Request $request)
    {
        Question::fixTree();

        if ($request->name)
        {
            $question = Question::create($request->all());

            session()->flash('success', 'The new company or department has been added');
        }

        return redirect()->route('admin.questions');
    }
}
