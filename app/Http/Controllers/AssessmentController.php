<?php

namespace App\Http\Controllers;

use App\Models\Assessment\Assessment;
use Auth;
use Illuminate\Http\Request;

class AssessmentController extends Controller
{
    public function assessment()
    {
        $args = [];
        $args['assessment'] = Assessment::where('user_id', Auth::id())->where('is_complete', 0)->firstOrCreate(['user_id' => Auth::id()]);
        $args['question']   = $args['assessment']->getCurrentQuestion();

        return view('main.assessment.question', $args);
    }
}
