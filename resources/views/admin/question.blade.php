@extends('template')

@section('content')
    <h1>Edit Question</h1>
    
    {{ Form::model($question, ['route' => ['admin.question-post', $question->id], 'method' => 'post']) }}

        <div class="card">
            <div class="card-header">Main Question Details</div>
            <div class="card-body">
                {{ Form::bsText('Text of Question', 'name') }}
                {{ Form::bsNumber('Order', 'order') }}
                {{ Form::bsSelect('Parent (if applicable)', 'parent_id', $formParents, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Next Question', 'next_question_id', $formQuestions, null) }}
                {{ Form::bsSelect('Category', 'enum_assessment_category', $formCategories, null) }}
                {{ Form::bsText('Answer Type', 'answer_type') }}
                {{ Form::bsText('Possible Answers', 'possible_answers') }}
            </div>
        </div>

        <div class="card">
            <div class="card-header">Answer Details</div>
            <div class="card-body">
                {{-- Acceptable inputs are "date", "number", "selectOne", "selectMany", "text" --}}
                @if ('selectOne' === $question->answer_type || 'selectMany' === $question->answer_type)
                    <div class="row">
                        <div class="col-1">Delete?</div>
                        <div class="col-4">Answer</div>
                        <div class="col-4">Next Question</div>
                        <div class="col-3">Score</div>

                        @foreach ($question->answers as $answer)
                            <div class="col-1">{{ Form::checkbox('delete_answers[]', $answer->id, null, ['class' => 'form-check-input']) }}</div>
                            <div class="col-4">{{ $answer->name }}</div>
                            <div class="col-4">{{ $answer->specificNextQuestionText }}</div>
                            <div class="col-3">{{ $answer->score }}</div>
                        @endforeach
                    </div>
                @else
                    <p>This question is a date, number or text answer. As these are free inputs, there are no details to enter here.</p>
                @endif
            </div>
        </div>

        @if ('selectOne' === $question->answer_type || 'selectMany' === $question->answer_type)
            <div class="card">
                <div class="card-header">Add New Possible Answer</div>
                <div class="card-body">
                    {{ Form::bsText('Answer Text', 'answer_name') }}
                    {{ Form::bsText('Score', 'answer_score') }}
                    {{ Form::bsSelect('Next Question', 'answer_next_question_id', $formQuestions, null, ['placeholder' => '']) }}
                </div>
            </div>
        @endif
        
        {{ Form::bsSubmit('Update Question') }}

    {{ Form::close() }}
@endsection