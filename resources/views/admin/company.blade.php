@extends('template')

@section('content')
    <h1>Companies</h1>
    @foreach ($companies as $company)
        <p>{{ $company->name }}</p>

        @foreach ($company->children as $childCompany)
            <p>-- {{ $childCompany->name }}</p>
        @endforeach
    @endforeach

    {{ Form::open(['route' => 'admin.company-post', 'method' => 'post']) }}
        {{ Form::bsText('New Company or Department Name', 'name') }}
        {{ Form::bsSelect('Parent (if applicable)', 'parent_id', $formCompanies, null, ['placeholder' => '']) }}
        {{ Form::bsSubmit('Add New Company / Department') }}
    {{ Form::close() }}
@endsection