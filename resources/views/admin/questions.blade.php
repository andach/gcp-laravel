@extends('template')

@section('content')
    <h1>Questions</h1>
    @foreach ($questions as $question)
        <p><a href="{{ route('admin.question', $question->id) }}">{{ $question->name }}</a></p>

        @foreach ($question->children as $childQuestion)
            <p>-- <a href="{{ route('admin.question', $childQuestion->id) }}">{{ $childQuestion->name }}</a></p>
        @endforeach
    @endforeach

    {{ Form::open(['route' => 'admin.questions-post', 'method' => 'post']) }}
        {{ Form::bsText('Text of Question', 'name') }}
        {{ Form::bsNumber('Order', 'order') }}
        {{ Form::bsSelect('Parent (if applicable)', 'parent_id', $formParents, null, ['placeholder' => '']) }}
        {{ Form::bsSelect('Next Question', 'next_question_id', $formQuestions, null, ['helpBlock' => 'This is the default next question, and can be overriden by specific answers in a later screen.']) }}
        {{ Form::bsSelect('Category', 'enum_assessment_category', $formCategories, null) }}
        {{ Form::bsText('Answer Type', 'answer_type', null, ['helpBlock' => 'Acceptable inputs are "date", "number", "selectOne", "selectMany", "text"']) }}
        {{ Form::bsText('Possible Answers', 'possible_answers') }}
        {{ Form::bsSubmit('Add New Question') }}
    {{ Form::close() }}
@endsection