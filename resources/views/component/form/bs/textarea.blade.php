@php
    $class = 'form-control';
    if ($errors->has($name))
    {
        $class = 'form-control is-invalid';
    }

    $labelColumns   = $attributes['labelCols'] ?? 'col-12';
    $elementColumns = $attributes['elementCols'] ?? 'col-12';
    $helpBlockName  = $name.'HelpBlock';
@endphp

<div class="form-group row">
    {{ Form::label($title, null, ['class' => $labelColumns.' col-form-label']) }}
    <div class="{{ $elementColumns }}">
        @if (old($name))
            {{ Form::textarea($name, old($name), array_merge(['class' => $class, 'rows' => 5], $attributes)) }}
        @else
            {{ Form::textarea($name, $value, array_merge(['class' => $class, 'rows' => 5], $attributes)) }}
        @endif

        @if (isset($attributes['helpBlock']))
            <small id="{{ $helpBlockName }}" class="form-text">
                {!! $attributes['helpBlock'] !!}
            </small>
        @endif

        @if ($errors->has($name))
            <div class="invalid-feedback">
                Error: {{ $errors->first($name) }}
            </div>
        @endif
    </div>
</div>
