@php
    $class = 'form-control';
    if ($errors->has($name))
    {
        $class = 'form-control is-invalid';
    }

    $labelColumns   = $attributes['labelCols'] ?? 'col-12 col-md-3';
    $elementColumns = $attributes['elementCols'] ?? 'col-12 col-md-9';
    $helpBlockName  = $name.'HelpBlock';
@endphp

<div class="form-group row">
    {{ Form::label($title, null, ['class' => $labelColumns.' col-form-label']) }}
    <div class="{{ $elementColumns }}">
        @if (old($name))
            {{ Form::select($name, $options, old($name), array_merge(['class' => $class], $attributes)) }}
        @else
            {{ Form::select($name, $options, $value, array_merge(['class' => $class], $attributes)) }}
        @endif

        @if (isset($attributes['helpBlock']))
            <small id="{{ $helpBlockName }}" class="form-text">
                {!! $attributes['helpBlock'] !!}
            </small>
        @endif

        @if ($errors->has($name))
            <div class="invalid-feedback">
                Error: {{ $errors->first($name) }}
            </div>
        @endif
    </div>
</div>
