@extends('template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        {{ Form::bsText('Email Address', 'email') }}
                        {{ Form::bsPassword('Password', 'password') }}
                        {{ Form::bsCheckbox('Remember Me?', 'remember') }}
                        {{ Form::bsSubmit('Login', 'btn-success') }}

                        {{-- Login with Facebook --}}
                        <div class="flex items-center justify-end mt-4">
                            <a class="btn" href="{{ url('auth/facebook/redirect') }}"
                                style="background: #3B5499; color: #ffffff; padding: 10px; width: 100%; text-align: center; display: block; border-radius:3px;">
                                Login with Facebook
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
