
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>GCP Curve II</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/app.css" />
    </head>

    <body>
        <!-- Responsive navbar-->
        @include('nav')
        
        <!-- Page content-->
        <div id="mainContainer" class="container">
            @if (Session::has('success'))
                <div class="alert alert-success"><b>Success:</b> {!! Session::get('success') !!}</div>
            @endif

            @yield('content')
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
    </body>

    <footer>Copyright GCP</footer>
</html>
